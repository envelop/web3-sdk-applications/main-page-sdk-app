import {
	BigNumber,
	combineURLs,
	createAuthToken
} from "@envelop/envelop-client-core";

const camelCaseToWords = (s: string) => {
	const result = s.replace(/([A-Z])/g, ' $1');
	return result.charAt(0).toUpperCase() + result.slice(1);
}

export const getPointsBalanceMultiple = async ( contractAddresses: Array<string>, pointsTypeNames: Array<string> ) => {

	let output: Array<{ key: string, value: string }> = [];

	for (let idx = 0; idx < contractAddresses.length; idx++) {
		const item = contractAddresses[idx];

		try {
			const fetchedBalances = await getPointsBalance(item, pointsTypeNames);
			fetchedBalances.forEach((iitem) => {
				let valueToSum = new BigNumber(0);
				const existsInOutput = output.find((iiitem) => { return iitem.key.toLowerCase() === iiitem.key.toLowerCase() });
				if ( existsInOutput ) { valueToSum = new BigNumber(existsInOutput.value) }

				const summedValue = valueToSum.plus(new BigNumber(iitem.value));

				output = [
					...output.filter((iiitem) => { return iitem.key.toLowerCase() !== iiitem.key.toLowerCase() }),
					{ key: iitem.key, value: summedValue.toString() }
				];
			});

		} catch(e) {
			throw new Error(`Cannot fetch blastpoints balance for ${item}: ${e}`);
		}
	}

	return output;

}
export const getPointsBalance = async ( contractAddress: string, pointsTypeNames: Array<string> ) => {

	const authToken = await createAuthToken();
	if ( authToken === '' ) {
		console.log('Cannot create token');
		throw new Error('Cannot create token');
	}
	const BASE_URL = process.env.REACT_APP_ORACLE_API_BASE_URL;
	if ( !BASE_URL ) {
		console.log('No oracle base url in .env');
		throw new Error('No oracle base url in .env');
	}

	const url = combineURLs(BASE_URL, `/blastpoints/balance/${contractAddress}`);

	let respParsed: any;
	try {
		const resp = await fetch(url, {
			headers: {
				'Authorization': authToken,
			}
		});

		if ( resp && resp.ok ) {
			respParsed = await resp.json();
		}
	} catch (e) {
		return [];
	}

	if ( !respParsed ) {
		return [];
	}

	const output: Array<{ key: string, value: string }> = [];

	try {
		const balanceGroupNames = Object.keys(respParsed.balancesByPointType);
		balanceGroupNames.forEach((groupName) => {
			if ( !pointsTypeNames.includes(groupName.toLowerCase()) ) { return; }
			const balanceTypeNames = Object.keys(respParsed.balancesByPointType[groupName]);

			balanceTypeNames.forEach((pointType) => {
				if ( typeof(respParsed.balancesByPointType[groupName][pointType]) !== 'string' ) { return; }

				const value  = parseInt(respParsed.balancesByPointType[groupName][pointType]);
				if ( value !== 0 ) { output.push({ key: camelCaseToWords(pointType), value: `${value}` }); }
			})
		})
	} catch(e) {
		console.log('Cannot parse blastpoints response', e);
	}

	return output;

}