import React from 'react';
import App from './components/App';
import 'tippy.js/dist/tippy.css';
import './static/css/styles.min.css';

import { BrowserRouter, Route, Routes } from 'react-router-dom';

import { createRoot } from 'react-dom/client';
import ErrorPage from './components/ErrorPage';

console.log(`Envelop SDK v${require("@envelop/envelop-client-core/package.json").version}`);

const container = document.getElementById('root');

const root = createRoot(container!); // createRoot(container!) if you use TypeScript
root.render(
	<React.StrictMode>
		<BrowserRouter basename={process.env.PUBLIC_URL}>
			<Routes>
				<Route path="/" element={ <App /> } />
				<Route path="*" element={ <ErrorPage /> } />
			</Routes>
		</BrowserRouter>
	</React.StrictMode>,
);