
import 'tippy.js/dist/tippy.css';

import Header from '../Header';
import Footer from '../Footer';

import MainPage from '../MainPage';
import { Web3Dispatcher } from '../../dispatchers';

export default function App() {

	return (
		<>
		<Web3Dispatcher>
			<Header />
			<MainPage />
			<Footer />
		</Web3Dispatcher>
		</>
	)

}