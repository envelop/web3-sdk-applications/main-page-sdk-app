
import CollateralViewer, {
    CollateralError
} from './collateralviewer';

export type {
    CollateralError,
}

export default CollateralViewer;